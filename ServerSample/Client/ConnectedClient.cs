﻿using Core;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerSample.Client;

public class ConnectedClient
{
    /// <summary>
    /// 연결된 클라이언트 소켓 객체를 나타냅니다.
    /// </summary>
    public Socket clientSocket { get; private set; }

	/// <summary>
	/// 로그인 상태를 나타냅니다.
	/// </summary>
	public bool isLogin { get; private set; }

	/// <summary>
	/// 이 클라이언트에 사용되는 닉네임
	/// </summary>
	public string nickname { get; private set; }

    /// <summary>
	/// 이 클라이언트의 연결이 종료되었을 경우 발생하는 이벤트
	/// </summary>
    public event Action<ConnectedClient> onDisconnected;

	/// <summary>
	/// 이 객체에서 서버쪽으로 쿼리문 요청시에 사용될 함수
	/// </summary>
	public event Action<string, Func<MySqlDataReader, Task>> requestQueryCmd;

	/// <summary>
	/// 공지된 내용이 존재하는 경우 발생하는 이벤트
	/// </summary>
	public event Func<ConnectedClient/*client*/, PacketType/*packetType*/, object/*context*/, Task/*retVal*/> onAnnounced;

	/// <summary>
	/// 금칙어들을 저장할 Dictionary
	/// </summary>
	private Dictionary<string, string> _ForbiddenWords = new();


	public ConnectedClient(Socket clientSocket)
    {
        this.clientSocket = clientSocket;

		// 금칙어 초기화
		InitializeForbiddenWords();

		Console.WriteLine("클라이언트 연결됨!" +
            $"[{clientSocket.RemoteEndPoint}]");

        // 클라이언트 데이터 수신 시작
        new Thread(DoListen).Start();
    }
	

    public async void DoListen()
    {
        try
        {
            while (true)
            {

				// 패킷을 받습니다.
				PacketType packetType = await Packet.ReceiveAsyncPacketType(clientSocket);



				// 패킷 타입에 따라 데이터를 처리합니다.
				// 연결이 종료된 경우
				if (packetType == PacketType.IsDisconnected)
				{
					isLogin = false;

					// 연결 종료 이벤트 발생
					onDisconnected?.Invoke(this);
				}

				// 회원 가입 요청을 받은 경우
				else if (packetType == PacketType.RegisterRequest)
				{
					// 요청을 받습니다.
					RegisterRequestPacket packet =
						await Packet.ReceiveAsyncHeaderAndData<RegisterRequestPacket>(clientSocket);



					// 요청에 대한 처리를 진행합니다.
					requestQueryCmd(
						$"SELECT * FROM account_info",
						async (table) =>
						{
							// 아이디 중복 여부 확인
							bool isDuplicated = false; // 중복 여부를 나타내기 위한 변수

							// 길이가 너무 긴 경우 회원가입 실패
							if (packet.id.Length <= 10 &&
								packet.pw.Length <= 20 &&
								packet.nickname.Length <= 8)
							{
								while (table.Read())
								{
									// 이미 같은 ID 로 등록되어있는 경우
									if (table["id"].ToString() == packet.id ||
										// 이미 같은 닉네임으로 등록되어있는 경우
										table["nickname"].ToString() == packet.nickname)
									{
										isDuplicated = true;
										break;
									}
								}
							}
							else isDuplicated = true;


							if (packet.nickname.Contains("진송") ||
							packet.nickname.Contains("강사") ||
							packet.nickname.Contains("원장") ||
							packet.nickname.Contains("부장") ||
							packet.nickname.Contains("대리") ||
							packet.nickname.Contains("서율") ||
							packet.nickname.Contains("장염") ||
							packet.nickname.Contains("장트러블")) isDuplicated = true;


							// 중복된 아이디가 없는 경우
							if (!isDuplicated)
							{
								// 요청된 내용을 Table 에 추가합니다.
								requestQueryCmd(
									$"INSERT INTO account_info (id, pw, nickname) VALUES " +
									$"(\'{packet.id}\', \'{packet.pw}\', \'{packet.nickname}\');", null);
							}

							// 회원가입 성공 결과를 클라이언트에 전달
							await Packet.SendAsync(
								clientSocket, PacketType.RegisterResponse,
								new SimpleResponsePacket(!isDuplicated));
						});




				}

				// 로그인 요청을 받은 경우
				else if (packetType == PacketType.LoginRequest)
				{
					// 로그인 요청 데이터를 얻습니다.
					LoginRequestPacket packet =
						await Packet.ReceiveAsyncHeaderAndData<LoginRequestPacket>(clientSocket);

					requestQueryCmd($"SELECT * FROM account_info",
						async (table) =>
						{
							string nickname = default;

							while(table.Read())
							{
								// 요청된 id, pw 와 일치하는 계정이 존재한다면
								if (table["id"].ToString() == packet.id)
								{
									if (table["pw"].ToString() == packet.pw)
									{
										// 닉네임 설정
										nickname = table["nickname"].ToString();

										// 로그인 상태로 변경합니다.
										isLogin = true;

										Console.WriteLine($"{packet.id} 로그인되었음.");
									}

									break;
								}
							}

							// 보낼 데이터를 생성합니다.
							LoginResponsePacket response = new(nickname, isLogin);

							// 로그인 결과를 보냅니다.
							await Packet.SendAsync(
								clientSocket, PacketType.LoginResponse,
								response);

							// 로그인에 성공한 경우, 다른 클라이언트에게 내용을 공지합니다.
							if (isLogin)
							{
								// 닉네임 설정
								this.nickname = response.nickname;

								await onAnnounced(this, PacketType.OnNewUserLogin, response);
							}

						});
				}

				// 새로운 채팅이 요청된 경우
				else if (packetType == PacketType.OnNewChattingRequest)
				{
					// 요청된 채팅 내용을 얻습니다.
					NewChattingContext packet = 
						await Packet.ReceiveAsyncHeaderAndData<NewChattingContext>(clientSocket);

					// 채팅 필터링
					Filtering(ref packet);

					// 발신자를 포함한 다른 클라이언트들에게 내용을 전달합니다.
					await onAnnounced(this, PacketType.OnNewChattingAdded, packet);
				}
            }
        }
        catch (ObjectDisposedException)
        {
            // 클라이언트 연결 해제
            onDisconnected?.Invoke(this);
        }
        catch (Exception)
        {
            // 클라이언트 연결 해제
            onDisconnected?.Invoke(this);
        }
    }

	/// <summary>
	/// 금칙어 초기화
	/// </summary>
	private void InitializeForbiddenWords()
	{
		_ForbiddenWords.Add("시발", "저런");
		_ForbiddenWords.Add("병신", "나쁜");
		_ForbiddenWords.Add("즐", "뿡");
		_ForbiddenWords.Add("장트러블", "장염");
		_ForbiddenWords.Add("똥", "뿅");
	}


	private void Filtering(ref NewChattingContext chattingContext)
	{
		// 채팅 문자열을 얻습니다.
		string context = chattingContext.context;

		foreach(KeyValuePair<string, string> elem in _ForbiddenWords)
		{
			// 금칙어가 포함된 문자열인 경우\
			string check = elem.Key;
			while (context.Contains(check))
			{
				context = context.Replace(check, elem.Value);
			}
		}

		chattingContext.context = context;
	}

}
