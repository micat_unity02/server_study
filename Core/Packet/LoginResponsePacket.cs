﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	/// <summary>
	/// 아주 간단한 결과를 반환하기 위하여 사용되는 형식입니다.
	/// </summary>
	public struct LoginResponsePacket
	{
		/// <summary>
		/// 닉네임
		/// </summary>
		public string nickname;

		/// <summary>
		/// 결과를 나타냅니다.
		/// </summary>
		public bool result;

		/// <summary>
		/// 설명자입니다.
		/// </summary>
		public string context;

		/// <summary>
		/// 다른 유저 닉네임
		/// </summary>
		public List<string> otherUserNicknames;

		public LoginResponsePacket(
			string nickname, 
			bool result, 
			List<string> otherUserNicknames = null, 
			string context = null)
		{
			this.nickname = nickname;
			this.result = result;
			this.otherUserNicknames = otherUserNicknames;
			this.context = context;
		}

	}
}
