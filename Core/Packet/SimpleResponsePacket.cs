﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	/// <summary>
	/// 아주 간단한 결과를 반환하기 위하여 사용되는 형식입니다.
	/// </summary>
	public struct SimpleResponsePacket
	{
		/// <summary>
		/// 결과를 나타냅니다.
		/// </summary>
		public bool result;

		/// <summary>
		/// 설명자입니다.
		/// </summary>
		public string context;

		public SimpleResponsePacket(bool result, string context = null)
		{
			this.result = result;
			this.context = context;
		}

	}
}
