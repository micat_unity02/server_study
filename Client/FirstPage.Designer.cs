﻿using System.Drawing;
using System.Windows.Forms;

namespace ClientSample
{
	partial class FirstPage
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Label_Result = new Label();
			Button_Connect = new Button();
			SuspendLayout();
			// 
			// Label_Result
			// 
			Label_Result.AutoSize = true;
			Label_Result.Font = new Font("맑은 고딕", 20.25F, FontStyle.Regular, GraphicsUnit.Point);
			Label_Result.Location = new Point(308, 131);
			Label_Result.Name = "Label_Result";
			Label_Result.Size = new Size(194, 37);
			Label_Result.TabIndex = 0;
			Label_Result.Text = "연결되지 않음.";
			// 
			// Button_Connect
			// 
			Button_Connect.Location = new Point(368, 221);
			Button_Connect.Name = "Button_Connect";
			Button_Connect.Size = new Size(75, 23);
			Button_Connect.TabIndex = 1;
			Button_Connect.Text = "연결";
			Button_Connect.UseVisualStyleBackColor = true;
			// 
			// FirstPage
			// 
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(800, 450);
			Controls.Add(Button_Connect);
			Controls.Add(Label_Result);
			Name = "FirstPage";
			Text = "Form1";
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private Label Label_Result;
		private Button Button_Connect;
	}
}
